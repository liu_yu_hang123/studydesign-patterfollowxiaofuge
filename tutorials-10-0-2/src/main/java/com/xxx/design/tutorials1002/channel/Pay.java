package com.xxx.design.tutorials1002.channel;

import com.xxx.design.tutorials1002.mode.IPayMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * @author Hang
 * @date 2022/7/31
 * @project studydesignpatter
 * @description 模式抽象类
 **/
public abstract class Pay {

    protected Logger logger = LoggerFactory.getLogger(Pay.class);

    protected IPayMode payMode;

    public Pay(IPayMode payMode) {
        this.payMode = payMode;
    }

    public abstract String transfer(String uId, String tradeId, BigDecimal amount);

}