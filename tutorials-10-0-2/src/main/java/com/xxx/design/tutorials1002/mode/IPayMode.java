package com.xxx.design.tutorials1002.mode;

/**
 * @author Hang
 * @date 2022/7/31
 * @project studydesignpatter
 * @description
 **/
public interface IPayMode {
    boolean security(String uId);
}
