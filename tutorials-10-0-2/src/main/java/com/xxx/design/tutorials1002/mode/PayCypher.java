package com.xxx.design.tutorials1002.mode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Hang
 * @date 2022/7/31
 * @project studydesignpatter
 * @description
 **/
public class PayCypher implements IPayMode{
    protected Logger logger = LoggerFactory.getLogger(PayCypher.class);

    public boolean security(String uId) {
        logger.info("密码支付，风控校验环境安全");
        return true;
    }
}
