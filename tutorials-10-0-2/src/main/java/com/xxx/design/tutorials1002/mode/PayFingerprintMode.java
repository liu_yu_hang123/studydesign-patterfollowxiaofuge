package com.xxx.design.tutorials1002.mode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Hang
 * @date 2022/7/31
 * @project studydesignpatter
 * @description
 **/
public class PayFingerprintMode implements IPayMode{
    protected Logger logger = LoggerFactory.getLogger(PayCypher.class);

    public boolean security(String uId) {
        logger.info("指纹支付，风控校验指纹信息");
        return true;
    }

}
