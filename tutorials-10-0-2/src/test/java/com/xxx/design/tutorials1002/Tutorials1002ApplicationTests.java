package com.xxx.design.tutorials1002;

import com.xxx.design.tutorials1002.channel.Pay;
import com.xxx.design.tutorials1002.channel.WxPay;
import com.xxx.design.tutorials1002.channel.ZfbPay;
import com.xxx.design.tutorials1002.mode.PayFaceMode;
import com.xxx.design.tutorials1002.mode.PayFingerprintMode;
import org.junit.jupiter.api.Test;


import java.math.BigDecimal;

class Tutorials1002ApplicationTests {

    @Test
    void contextLoads() {
        System.out.println("\r\n模拟测试场景；微信支付、人脸方式。");
        Pay wxPay = new WxPay(new PayFaceMode());
        wxPay.transfer("weixin_1092033111", "100000109893", new BigDecimal(100));

        System.out.println("\r\n模拟测试场景；支付宝支付、指纹方式。");
        Pay zfbPay = new ZfbPay(new PayFingerprintMode());
        zfbPay.transfer("jlu19dlxo111","100000109894",new BigDecimal(100));
    }

}
