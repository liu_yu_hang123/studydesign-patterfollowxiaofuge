package com.xxx.design.tutorials1102.domain.model.aggregates;

import com.xxx.design.tutorials1102.domain.model.vo.TreeNode;
import com.xxx.design.tutorials1102.domain.model.vo.TreeRoot;

import java.util.Map;

/**
 * @author Hang
 * @date 2022/7/31
 * @project studydesignpatter
 * @description 规则树聚合
 **/
public class TreeRich {
    private TreeRoot treeRoot;                          //树根信息
    private Map<Long, TreeNode> treeNodeMap;        //树节点ID -> 子节点

    public TreeRich(TreeRoot treeRoot, Map<Long, TreeNode> treeNodeMap) {
        this.treeRoot = treeRoot;
        this.treeNodeMap = treeNodeMap;
    }

    public TreeRoot getTreeRoot() {
        return treeRoot;
    }

    public void setTreeRoot(TreeRoot treeRoot) {
        this.treeRoot = treeRoot;
    }

    public Map<Long, TreeNode> getTreeNodeMap() {
        return treeNodeMap;
    }

    public void setTreeNodeMap(Map<Long, TreeNode> treeNodeMap) {
        this.treeNodeMap = treeNodeMap;
    }
}
