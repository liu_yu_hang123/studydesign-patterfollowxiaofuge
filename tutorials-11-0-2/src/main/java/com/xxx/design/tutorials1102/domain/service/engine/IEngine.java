package com.xxx.design.tutorials1102.domain.service.engine;

import com.xxx.design.tutorials1102.domain.model.aggregates.TreeRich;
import com.xxx.design.tutorials1102.domain.model.vo.EngineResult;

import java.util.Map;

/**
 * @author Hang
 * @date 2022/7/31
 * @project studydesignpatter
 * @description
 **/
public interface IEngine {
    EngineResult process(final Long treeId, final String userId, TreeRich treeRich, final Map<String, String> decisionMatter);
}
