package com.xxx.design.tutorials1102.domain.service.logic.impl;

import com.xxx.design.tutorials1102.domain.service.logic.BaseLogic;

import java.util.Map;

/**
 * @author Hang
 * @date 2022/7/31
 * @project studydesignpatter
 * @description
 **/
public class UserAgeFilter extends BaseLogic {

    @Override
    public String matterValue(Long treeId, String userId, Map<String, String> decisionMatter) {
        return decisionMatter.get("age");
    }

}
