package com.xxx.design.tutorials1200;

/**
 * @author Hang
 * @date 2022/7/31
 * @project studydesignpatter
 * @description
 **/
public interface HandlerInterceptor {
        boolean preHandle(String request,String response,Object handler);
}
