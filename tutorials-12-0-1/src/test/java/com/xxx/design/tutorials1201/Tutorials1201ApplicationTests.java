package com.xxx.design.tutorials1201;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

class Tutorials1201ApplicationTests {

    @Test
    public void test_LoginSsoDecorator() {
        LoginSsoDecorator ssoDecorator = new LoginSsoDecorator();
        String request = "1successhuahua";
        boolean success = ssoDecorator.preHandle(request, "ewcdqwt40liuiu", "t");
        System.out.println("登录校验：" + request + (success ? " 放行" : " 拦截"));
    }

}
