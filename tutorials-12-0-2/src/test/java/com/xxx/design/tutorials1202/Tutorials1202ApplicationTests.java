package com.xxx.design.tutorials1202;

import com.xxx.design.tutorials1200.SsoInterceptor;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


class Tutorials1202ApplicationTests {

    @Test
    public void test_LoginSsoDecorator() {
        // 这里通过传递原有的sso拦截来实现装饰器中的具体实现的添加扩充认证功能
        LoginSsoDecorator ssoDecorator = new LoginSsoDecorator(new SsoInterceptor());
        String request = "1successhuahua";
        boolean success = ssoDecorator.preHandle(request, "ewcdqwt40liuiu", "t");
        System.out.println("登录校验：" + request + (success ? " 放行" : " 拦截"));
    }

}
