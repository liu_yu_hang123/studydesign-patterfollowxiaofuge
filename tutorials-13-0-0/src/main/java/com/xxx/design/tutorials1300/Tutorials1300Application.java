package com.xxx.design.tutorials1300;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
public class Tutorials1300Application {

    public static void main(String[] args) {
        SpringApplication.run(Tutorials1300Application.class, args);
    }

}
