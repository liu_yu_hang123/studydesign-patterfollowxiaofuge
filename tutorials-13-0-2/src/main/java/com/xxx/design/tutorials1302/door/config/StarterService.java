package com.xxx.design.tutorials1302.door.config;

import org.springframework.util.StringUtils;
// 切面定义
public class StarterService {

    private String userStr;

    public StarterService(String userStr) {
        this.userStr = userStr;
    }

    public String[] split(String separatorChar) {
        return StringUtils.split(this.userStr, separatorChar);
    }

}