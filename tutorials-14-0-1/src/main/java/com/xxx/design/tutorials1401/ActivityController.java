package com.xxx.design.tutorials1401;

import com.xxx.design.tutorials1402.Activity;
import com.xxx.design.tutorials1402.Stock;

import java.util.Date;

public class ActivityController {

    public Activity queryActivityInfo(Long id) {
        // 模拟从实际业务应用从接口中获取活动信息，每次调用都需要从库存中获取信息，构造对象并返回
        Activity activity = new Activity();
        activity.setId(10001L);
        activity.setName("图书嗨乐");
        activity.setDesc("图书优惠券分享激励分享活动第二期");
        activity.setStartTime(new Date());
        activity.setStopTime(new Date());
        activity.setStock(new Stock(1000,1));
        return activity;
    }

}