package com.xxx.design.tutorials1500;


import com.xxx.design.tutorials1500.agent.Select;

public interface IUserDao {

    @Select("select userName from user where id = #{uId}")
    String queryUserInfo(String uId);

}