package com.xxx.design.tutorials1702.cook;

/**
 * @author Hang
 * @date 2022/8/2
 * @project studydesignpatter
 * @description
 **/
public interface ICook {

    void doCooking();

}
