package com.xxx.design.tutorials1702.cuisine;

/**
 * @author Hang
 * @date 2022/8/2
 * @project studydesignpatter
 * @description
 **/
/**
 * 菜系：山东（鲁菜） 四川（川菜） 江苏（苏菜） 广东（粤菜） 福建（闽菜） 浙江（浙菜） 湖南（湘菜） 安徽（徽菜）
 */
public interface ICuisine {

    void cook(); // 烹调、制作

}