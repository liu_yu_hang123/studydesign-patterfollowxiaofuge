package com.xxx.design.tutorials1702;

import com.xxx.design.tutorials1702.cook.impl.GuangDongCook;
import com.xxx.design.tutorials1702.cook.impl.JiangSuCook;
import com.xxx.design.tutorials1702.cook.impl.ShanDongCook;
import com.xxx.design.tutorials1702.cook.impl.SiChuanCook;
import com.xxx.design.tutorials1702.cuisine.ICuisine;
import com.xxx.design.tutorials1702.cuisine.impl.GuangDoneCuisine;
import com.xxx.design.tutorials1702.cuisine.impl.JiangSuCuisine;
import com.xxx.design.tutorials1702.cuisine.impl.ShanDongCuisine;
import com.xxx.design.tutorials1702.cuisine.impl.SiChuanCuisine;
import org.junit.jupiter.api.Test;

/**
 * 博客：https://bugstack.cn - 沉淀、分享、成长，让自己和他人都能有所收获！
 * 公众号：bugstack虫洞栈
 * Create by 小傅哥(fustack) @2020
 */
public class ApiTest {

    @Test
    public void test_xiaoEr(){

        // 菜系 + 厨师；广东（粤菜）、江苏（苏菜）、山东（鲁菜）、四川（川菜）
        ICuisine guangDoneCuisine = new GuangDoneCuisine(new GuangDongCook());
        JiangSuCuisine jiangSuCuisine = new JiangSuCuisine(new JiangSuCook());
        ShanDongCuisine shanDongCuisine = new ShanDongCuisine(new ShanDongCook());
        SiChuanCuisine siChuanCuisine = new SiChuanCuisine(new SiChuanCook());

        // 点单
        XiaoEr xiaoEr = new XiaoEr();
        xiaoEr.order(guangDoneCuisine);
        xiaoEr.order(jiangSuCuisine);
        xiaoEr.order(shanDongCuisine);
        xiaoEr.order(siChuanCuisine);

        // 下单
        xiaoEr.placeOrder();

    }

}