package com.xxx.design.tutorials1800.lang;

public interface Iterable<E> {

    Iterator<E> iterator();

}