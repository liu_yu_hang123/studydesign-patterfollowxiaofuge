package com.xxx.design.tutorials1800.lang;

public interface Iterator<E> {

    boolean hasNext();

    E next();
    
}