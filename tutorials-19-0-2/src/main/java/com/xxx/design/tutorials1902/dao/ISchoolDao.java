package com.xxx.design.tutorials1902.dao;

import com.xxx.design.tutorials1902.po.School;

public interface ISchoolDao {

    School querySchoolInfoById(Long treeId);

}