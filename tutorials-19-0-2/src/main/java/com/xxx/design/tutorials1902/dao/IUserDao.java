package com.xxx.design.tutorials1902.dao;

import com.xxx.design.tutorials1902.po.User;

public interface IUserDao {

     User queryUserInfoById(Long id);

}