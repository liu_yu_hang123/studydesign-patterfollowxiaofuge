package com.xxx.design.tutorials1902.mediator;

/**
 * 公众号 | bugstack虫洞栈
 * 博 客 | https://bugstack.cn
 * Create by 小傅哥 @2020
 */
public interface SqlSessionFactory {

    SqlSession openSession();

}