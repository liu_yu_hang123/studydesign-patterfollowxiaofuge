package com.example.tutorials210;


import org.junit.Test;

/**
 * @author Hang
 * @date 2022/7/28
 * @project studydesignpatter
 * @description 功能测试
 **/
public class ApiTest {
    @Test
    public void test_serveGrade(){
        VideoUserService service = new VideoUserService();
        service.serveGrade("VIP用户");
        service.serveGrade("普通用户");
        service.serveGrade("访客用户");
    }
}
