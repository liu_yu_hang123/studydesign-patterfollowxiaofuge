package com.xxx.design.tutorials211;

/**
 * @author Hang
 * @date 2022/7/26
 * @project studydesignpatter
 * @description  单一接口原则版本
 **/
public interface IVideoUserServices {
    void definition();
    void advertisement();
}
