package com.xxx.design.tutorials211.impl;

import com.xxx.design.tutorials211.IVideoUserServices;

/**
 * @author Hang
 * @date 2022/7/26
 * @project studydesignpatter
 * @description 访客用户
 **/
public class GuestVideoUserService implements IVideoUserServices {
    @Override
    public void definition() {
        System.out.println("访客用户，视频480p高清");
    }

    @Override
    public void advertisement() {
        System.out.println("访客用户，视频有广告");
    }
}
