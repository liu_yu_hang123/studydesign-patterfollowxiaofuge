package com.xxx.design.tutorials211.impl;


import com.xxx.design.tutorials211.IVideoUserServices;

/**
 * @author Hang
 * @date 2022/7/26
 * @project studydesignpatter
 * @description 普通用户访问
 **/
public class OrdinaryVideoUserService implements IVideoUserServices {
    @Override
    public void definition() {
        System.out.println("普通用户，视频720p超清");
    }

    @Override
    public void advertisement() {
        System.out.println("普通用户，视频有广告");
    }
}
