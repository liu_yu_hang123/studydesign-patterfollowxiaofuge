package com.xxx.design.tutorials211.impl;


import com.xxx.design.tutorials211.IVideoUserServices;

/**
 * @author Hang
 * @date 2022/7/26
 * @project studydesignpatter
 * @description VIP用户
 **/
public class VIPVideoUserService implements IVideoUserServices {
    @Override
    public void definition() {
        System.out.println("VIP用户，视频1080p蓝光");
    }

    @Override
    public void advertisement() {
        System.out.println("vip用户，视频无广告");
    }
}
