package com.xxx.design.tutorials211;

import com.xxx.design.tutorials211.impl.GuestVideoUserService;
import com.xxx.design.tutorials211.impl.OrdinaryVideoUserService;
import com.xxx.design.tutorials211.impl.VIPVideoUserService;


class ApplicationTests {
    public static void main(String[] args) {
        // 访客用户
        GuestVideoUserService guestVideoUserService = new GuestVideoUserService();
        guestVideoUserService.advertisement();
        guestVideoUserService.definition();
        // 普通用户
        OrdinaryVideoUserService ordinaryVideoUserService = new OrdinaryVideoUserService();
        ordinaryVideoUserService.definition();
        ordinaryVideoUserService.advertisement();
        // vip用户
        VIPVideoUserService vipVideoUserService = new VIPVideoUserService();
        vipVideoUserService.definition();
        vipVideoUserService.advertisement();
    }

}
