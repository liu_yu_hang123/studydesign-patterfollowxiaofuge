package com.xxx.design.tutorials220;

/**
 * @author Hang
 * @date 2022/7/27
 * @project studydesignpatter
 * @description 面积计算接口
 **/
public interface ICalculationArea {
    /**
    * @Description: 计算长方形面积
    * @Param: [double, double]
    * @return: double
    * @Author: Hang
    * @Date: 2022/7/27 21:45
    */
    double rectangle(double x,double y);

    /**
    * @Description: 计算三角形面积
    * @Param: [double, double, double]
    * @return: double
    * @Author: yuhang
    * @Date: 2022/7/27 21:42
    */
    double triangle(double x,double y,double z);

    /**
    * @Description: 计算圆形面积
    * @Param: [double]
    * @return: double
    * @Author: yuhang
    * @Date: 2022/7/27 21:43
    */
    double circular(double r);
}
