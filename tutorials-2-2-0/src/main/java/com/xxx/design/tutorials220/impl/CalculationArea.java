package com.xxx.design.tutorials220.impl;


import com.xxx.design.tutorials220.ICalculationArea;

/**
 * @author Hang
 * @date 2022/7/27
 * @project studydesignpatter
 * @description 面积计算实现
 **/
public class CalculationArea implements ICalculationArea {
    private  final static double pi = 3.14D;
    @Override
    public double rectangle(double x, double y) {
        return x * y;
    }

    @Override
    public double triangle(double x, double y, double z) {
        double p = (x+y+z)/2;
        return Math.sqrt(p*(p-z)*(p-x)*(p-y));
    }

    @Override
    public double circular(double r) {
        return pi*r*r;
    }
}
