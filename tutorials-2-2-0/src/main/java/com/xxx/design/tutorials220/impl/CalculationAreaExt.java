package com.xxx.design.tutorials220.impl;

/**
 * @author Hang
 * @date 2022/7/27
 * @project studydesignpatter
 * @description 扩展继承，实现圆形的面积计算
 **/
public class CalculationAreaExt extends CalculationArea{
    private final static double pi = 3.141592653D;

    @Override
    public double circular(double r) {
        return pi * r * r;
    }
}
