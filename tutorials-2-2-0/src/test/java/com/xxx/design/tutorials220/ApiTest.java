package com.xxx.design.tutorials220;


import com.xxx.design.tutorials220.impl.CalculationArea;
import com.xxx.design.tutorials220.impl.CalculationAreaExt;
import org.junit.jupiter.api.Test;

/**
 * @author Hang
 * @date 2022/7/27
 * @project studydesignpatter
 * @description 功能测试
 **/
public class ApiTest {
    @Test
    public void test_CalculationAreaExt(){
        ICalculationArea calculationArea = new CalculationAreaExt();
        System.out.println(calculationArea.circular(10));
    }
    @Test
    public void test_CalculationArea(){
        ICalculationArea area = new CalculationArea();
        System.out.println(area.circular(10));
    }
}
