package com.xxx.design.tutorials230;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Hang
 * @date 2022/7/27
 * @project studydesignpatter
 * @description 模拟储蓄卡功能
 **/
public class CashCard {
    private Logger logger = LoggerFactory.getLogger(CashCard.class);

    /**
    * @Description: 提现
    * @Param: [java.lang.String, java.math.BigDecimal]
    * @return: java.lang.String
    * @Author: Hang
    * @Date: 2022/7/27 22:16
    */
    public String withdrawal(String orderId, BigDecimal amount){
        logger.info("提现成功，单号:{}，金额:{}",orderId,amount);
        return "0000";
    }

    /**
    * @Description: 储蓄
    * @Param: [java.lang.String, java.math.BigDecimal]
    * @return: java.lang.String
    * @Author: Hang
    * @Date: 2022/7/27 22:17
    */
    public String recharge(String orderId, BigDecimal amount) {
        // 模拟充值成功
        logger.info("储蓄成功，单号：{} 金额：{}", orderId, amount);
        return "0000";
    }

    /**
    * @Description:
    * @Param: []
    * @return: java.util.List<java.lang.String>
    * @Author: Hang
    * @Date: 2022/7/27 22:18
    */
    public List<String> tradeFlow() {
        logger.info("交易流水查询成功");
        List<String> tradeList = new ArrayList<String>();
        tradeList.add("100001,100.00");
        tradeList.add("100001,80.00");
        tradeList.add("100001,76.50");
        tradeList.add("100001,126.00");
        return tradeList;
    }
}
