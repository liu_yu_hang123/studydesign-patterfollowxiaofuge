package com.xxx.design.tutorials241;

import com.alibaba.fastjson.JSON;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @author Hang
 * @date 2022/7/27
 * @project studydesignpatter
 * @description 功能测试
 **/
public class Apitest {
    private Logger logger = LoggerFactory.getLogger(Apitest.class);

    @Test
    public void test_Principal() {
        Principal principal = new Principal();
        Map<String, Object> map = principal.queryClazzInfo("3年1班");
        logger.info("查询结果：{}", JSON.toJSONString(map));
    }
}
