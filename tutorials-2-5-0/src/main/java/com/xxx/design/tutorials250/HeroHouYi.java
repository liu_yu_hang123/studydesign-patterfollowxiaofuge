package com.xxx.design.tutorials250;

/**
 * @author Hang
 * @date 2022/7/28
 * @project studydesignpatter
 * @description 王者荣耀英雄 后裔
 **/
public class HeroHouYi implements ISkill{
    @Override
    public void doArchery() {
        System.out.println("后裔的灼日之矢");
    }

    @Override
    public void doInvisible() {
        System.out.println("后裔的隐身技能");
    }

    @Override
    public void doSilent() {
        System.out.println("后裔的沉默技能");
    }

    @Override
    public void doVertigo() {
        // 无此技能的实现
    }

}
