package com.xxx.design.tutorials250;

/**
 * @author Hang
 * @date 2022/7/28
 * @project studydesignpatter
 * @description 英雄技能
 **/
public interface ISkill {
    //灼日之矢
    void doArchery();

    // 隐袭
    void doInvisible();

    // 技能沉默
    void doSilent();

    // 眩晕
    void doVertigo();
}
