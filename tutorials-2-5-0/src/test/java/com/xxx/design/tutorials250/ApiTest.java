package com.xxx.design.tutorials250;

import org.junit.Test;

/**
 * @author Hang
 * @date 2022/7/28
 * @project studydesignpatter
 * @description 功能测试
 **/
public class ApiTest {
    @Test
    public void test_ISkill(){
        // 后裔
        HeroHouYi heroHouYi = new HeroHouYi();
        heroHouYi.doArchery();

        // 廉颇
        HeroLianPo heroLianPo = new HeroLianPo();
        heroLianPo.doInvisible();
    }
}
