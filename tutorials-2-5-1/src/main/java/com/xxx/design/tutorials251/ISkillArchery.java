package com.xxx.design.tutorials251;

/**
 * @author Hang
 * @date 2022/7/28
 * @project studydesignpatter
 * @description 射箭
 **/
public interface ISkillArchery {
    // 射箭
    void doArchery();
}
