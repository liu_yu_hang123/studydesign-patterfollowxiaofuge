package com.xxx.design.tutorials251;

/**
 * @author Hang
 * @date 2022/7/28
 * @project studydesignpatter
 * @description 不可视
 **/
public interface ISkillInvisible {
    // 隐袭
    void doInvisible();
}
