package com.xxx.design.tutorials251;

/**
 * @author Hang
 * @date 2022/7/28
 * @project studydesignpatter
 * @description 沉默
 **/
public interface ISkillSilent {
    // 沉默
    void doSilent();
}
