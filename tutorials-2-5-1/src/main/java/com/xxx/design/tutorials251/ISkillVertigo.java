package com.xxx.design.tutorials251;

/**
 * @author Hang
 * @date 2022/7/28
 * @project studydesignpatter
 * @description 眩晕
 **/
public interface ISkillVertigo {
    // 眩晕
    void doVertigo();
}
