package com.xxx.design.tutorials251.hero;

import com.xxx.design.tutorials251.ISkillArchery;
import com.xxx.design.tutorials251.ISkillInvisible;
import com.xxx.design.tutorials251.ISkillSilent;

/**
 * @author Hang
 * @date 2022/7/28
 * @project studydesignpatter
 * @description 王者英雄后裔
 **/
public class HeroHouYi implements ISkillArchery, ISkillSilent, ISkillInvisible {
    @Override
    public void doArchery() {
        System.out.println("后裔的灼日之矢");
    }

    @Override
    public void doInvisible() {
        System.out.println("后裔的隐身技能");
    }

    @Override
    public void doSilent() {
        System.out.println("后裔的沉默技能");
    }
}
