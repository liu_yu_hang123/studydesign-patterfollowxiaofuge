package com.xxx.design.tutorials251.hero;

import com.xxx.design.tutorials251.ISkillInvisible;
import com.xxx.design.tutorials251.ISkillSilent;
import com.xxx.design.tutorials251.ISkillVertigo;

/**
 * @author Hang
 * @date 2022/7/28
 * @project studydesignpatter
 * @description 王者英雄廉颇
 **/
public class HeroLianPo implements ISkillInvisible, ISkillSilent, ISkillVertigo {
    @Override
    public void doInvisible() {
        System.out.println("廉颇的隐身技能");
    }

    @Override
    public void doSilent() {
        System.out.println("廉颇的沉默技能");
    }

    @Override
    public void doVertigo() {
        System.out.println("廉颇的眩晕技能");
    }
}
