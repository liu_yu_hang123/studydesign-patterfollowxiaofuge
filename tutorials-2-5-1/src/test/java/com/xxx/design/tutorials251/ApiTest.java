package com.xxx.design.tutorials251;

import com.xxx.design.tutorials251.hero.HeroHouYi;
import com.xxx.design.tutorials251.hero.HeroLianPo;
import org.junit.Test;

/**
 * @author Hang
 * @date 2022/7/28
 * @project studydesignpatter
 * @description 功能测试
 **/
public class ApiTest {
    @Test
    public void test(){
        // 后裔
        HeroHouYi heroHouYi = new HeroHouYi();
        heroHouYi.doArchery();
        // 廉颇
        HeroLianPo heroLianPo = new HeroLianPo();
        heroLianPo.doInvisible();
    }
}
