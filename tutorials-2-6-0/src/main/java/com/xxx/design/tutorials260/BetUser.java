package com.xxx.design.tutorials260;

/**
 * @author Hang
 * @date 2022/7/28
 * @project studydesignpatter
 * @description 投注用户
 **/
public class BetUser {
    private String username; // 用户姓名
    private int userWeight; // 用户权重

    public BetUser() {
    }

    public BetUser(String username,int userWeight){
        this.username = username;
        this.userWeight = userWeight;
    }

    public String getUsername(){
        return username;
    }
    public void setUsername(String username){
        this.username = username;
    }
    public int getUserWeight(){
        return userWeight;
    }
    public void setUserWeight(int userWeight){
        this.userWeight = userWeight;
    }
}
