package com.xxx.design.tutorials261;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Hang
 * @date 2022/7/28
 * @project studydesignpatter
 * @description 抽奖控制
 **/
public class DrawControl {
    private IDraw draw;

    public List<BetUser> doDraw(IDraw draw,List<BetUser> betUserList,int count){
        return draw.prize(betUserList,count);
    }
}
