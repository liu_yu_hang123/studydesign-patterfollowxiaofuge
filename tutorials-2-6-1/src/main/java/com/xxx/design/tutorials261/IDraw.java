package com.xxx.design.tutorials261;

import java.util.List;

/**
 * @author Hang
 * @date 2022/7/28
 * @project studydesignpatter
 * @description
 **/
public interface IDraw {
    List<BetUser> prize(List<BetUser> list,int count);
}
