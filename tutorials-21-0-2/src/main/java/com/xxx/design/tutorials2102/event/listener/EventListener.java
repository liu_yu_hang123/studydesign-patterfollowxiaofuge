package com.xxx.design.tutorials2102.event.listener;

import com.xxx.design.tutorials2102.LotteryResult;

public interface EventListener {

    void doEvent(LotteryResult result);

}