package com.xxx.design.tutorials2400;

import com.xxx.design.tutorials2400.impl.JDNetMall;
import com.xxx.design.tutorials2400.impl.TaoBaoNetMall;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApiTest {

    public Logger logger = LoggerFactory.getLogger(ApiTest.class);

    /**
     * 测试链接
     * 京东；https://item.jd.com/100008348542.html
     * 淘宝；https://detail.tmall.com/item.htm
     * 当当；http://product.dangdang.com/1509704171.html
     */
    @Test
    public void test_NetMall() {
//        NetMall netMall = new JDNetMall("1000001","*******");
        TaoBaoNetMall haiwang = new TaoBaoNetMall("1000002", "haiwang");
//        String base64 = netMall.generateGoodsPoster("https://item.jd.com/100008348542.html");
        String base64 = haiwang.generateGoodsPoster("https://item.jd.com/100008348542.html");
        logger.info("测试结果：{}", base64);
    }

}