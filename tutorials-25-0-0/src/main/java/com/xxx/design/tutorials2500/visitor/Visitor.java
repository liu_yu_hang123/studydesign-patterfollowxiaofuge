package com.xxx.design.tutorials2500.visitor;

import com.xxx.design.tutorials2500.user.impl.Student;
import com.xxx.design.tutorials2500.user.impl.Teacher;

public interface Visitor {

    // 访问学生信息
    void visit(Student student);

    // 访问老师信息
    void visit(Teacher teacher);

}