package com.xxx.design.tutorials400.card;

/**
 * @author Hang
 * @date 2022/7/29
 * @project studydesignpatter
 * @description
 **/
/**
 * 模拟爱奇艺会员卡服务
 */
public class IQiYiCardService {

    public void grantToken(String bindMobileNumber, String cardId) {
        System.out.println("模拟发放爱奇艺会员卡一张：" + bindMobileNumber + "，" + cardId);
    }

}
