package com.xxx.design.tutorials401;

/**
 * @author Hang
 * @date 2022/7/29
 * @project studydesignpatter
 * @description
 **/
/**
 * 发奖结果反馈对象
 */
public class AwardRes {

    private String code; // 编码
    private String info; // 描述

    public AwardRes(String code, String info) {
        this.code = code;
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

}
