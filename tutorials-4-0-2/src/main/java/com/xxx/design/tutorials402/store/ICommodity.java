package com.xxx.design.tutorials402.store;

import java.util.Map;

/**
 * @author Hang
 * @date 2022/7/29
 * @project studydesignpatter
 * @description
 **/
public interface ICommodity {

    void sendCommodity(String uId, String commodityId, String bizId, Map<String, String> extMap) throws Exception;

}
