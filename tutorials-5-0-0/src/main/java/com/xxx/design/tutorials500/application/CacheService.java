package com.xxx.design.tutorials500.application;

import java.util.concurrent.TimeUnit;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description 定义接口
 **/
public interface CacheService {
    String get(final String key);

    void set(String key,String value);

    void set(String key, String value, long timeout, TimeUnit timeUnit);

    void del(String key);
}
