package com.xxx.design.tutorials500.application;

import com.xxx.design.tutorials500.redis.RedisUtils;

import java.util.concurrent.TimeUnit;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description 调用单机接口
 **/
public class CacheServiceImpl implements CacheService{
    private RedisUtils redisUtils;
    @Override
    public String get(String key) {
        return redisUtils.get(key);
    }

    @Override
    public void set(String key, String value) {
        redisUtils.set(key,value);
    }

    @Override
    public void set(String key, String value, long timeout, TimeUnit timeUnit) {
        redisUtils.set(key,value,timeout,timeUnit);
    }

    @Override
    public void del(String key) {
        redisUtils.del(key);
    }
}
