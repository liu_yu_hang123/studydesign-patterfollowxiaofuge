package com.xxx.design.tutorials500.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description 单机模式
 **/
public class RedisUtils {
    private Logger logger = LoggerFactory.getLogger(RedisUtils.class);
    private Map<String,String> dataMap = new ConcurrentHashMap<>();

    public String get(String key){
        logger.info("Redis 获取数据 key:{}",key);
        return dataMap.get(key);
    }

    public void set(String key,String value){
        logger.info("Redis写入数据key:{} val:{}",key,value);
        dataMap.put(key,value);
    }

    public void set(String key, String value, long timeout, TimeUnit timeUnit){
        logger.info("Redis写入数据key:{},value:{},timeout:{},timeUnit:{}");
        dataMap.put(key,value);
    }

    public void del(String key){
        logger.info("Redis删除数据key:{}",key);
        dataMap.remove(key);
    }
}
