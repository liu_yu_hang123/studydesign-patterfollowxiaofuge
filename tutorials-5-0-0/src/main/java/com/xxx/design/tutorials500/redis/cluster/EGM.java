package com.xxx.design.tutorials500.redis.cluster;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description EGM集群
 **/
public class EGM {
    private Logger logger = LoggerFactory.getLogger(EGM.class);

    private Map<String,String> dataMap = new ConcurrentHashMap<>();

    public String gain(String key){
        logger.info("EGM获取数据key:{}",key);
        return dataMap.get(key);
    }

    public void set(String key,String value){
        logger.info("EGM写入数据key:{},val:{}",key,value);
        dataMap.put(key,value);
    }

    public void setEx(String key, String value, long timeout, TimeUnit timeUnit){
        logger.info("EGM写入数据key:{},val:{},timeout:{},timeUnit:{}");
        dataMap.put(key,value);
    }

    public void delete(String key){
        logger.info("EGM删除数据key:{}",key);
        dataMap.remove(key);
    }
}
