package com.xxx.design.tutorials500.redis.cluster;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description IIR集群
 **/
public class IIR {
    private Logger logger = LoggerFactory.getLogger(EGM.class);

    private Map<String,String> dataMap = new ConcurrentHashMap<>();

    public String get(String key){
        logger.info("IIR获取数据key:{}",key);
        return dataMap.get(key);
    }

    public void set(String key,String value){
        logger.info("IIR写入数据key:{},val:{}",key,value);
        dataMap.put(key,value);
    }

    public void setExpire(String key, String value, long timeout, TimeUnit timeUnit){
        logger.info("IIR写入数据key:{},val:{},timeout:{},timeUnit:{}");
        dataMap.put(key,value);
    }

    public void del(String key){
        logger.info("IIR删除数据key:{}",key);
        dataMap.remove(key);
    }
}
