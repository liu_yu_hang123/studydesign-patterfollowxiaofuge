package com.xxx.design.tutorials502.factory;

import com.xxx.design.tutorials502.util.ClassLoaderUtils;
import com.xxx.design.tutorials502.workshop.ICacheAdapter;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description
 **/
public class JDKInvocationHandler implements InvocationHandler {
    private ICacheAdapter cacheAdapter;

    public JDKInvocationHandler(ICacheAdapter cacheAdapter) {
        this.cacheAdapter = cacheAdapter;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
<<<<<<< HEAD
=======
        // 传入代理类的方法名和代理类的参数类别，然后进行类别的invoke方法
>>>>>>> d2133f2 (第十一讲 理解并完成建造者模式的使用)
        return ICacheAdapter.class.getMethod(method.getName(), ClassLoaderUtils.getClazzByArgs(args)).invoke(cacheAdapter, args);
    }
}
