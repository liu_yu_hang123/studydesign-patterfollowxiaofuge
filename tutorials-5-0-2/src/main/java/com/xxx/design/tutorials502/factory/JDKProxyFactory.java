package com.xxx.design.tutorials502.factory;

import com.xxx.design.tutorials500.application.CacheServiceImpl;
import com.xxx.design.tutorials502.workshop.ICacheAdapter;
import com.xxx.design.tutorials502.workshop.impl.EGMCacheAdapter;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description
 **/
public class JDKProxyFactory {
    public static <T> T getProxy(Class<T> cacheClazz, Class<? extends ICacheAdapter> cacheAdapter) throws Exception {
        InvocationHandler handler = new JDKInvocationHandler(cacheAdapter.newInstance());
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        return (T) Proxy.newProxyInstance(classLoader, new Class[]{cacheClazz}, handler);
    }
}
