package com.xxx.design.tutorials502.workshop;

import java.util.concurrent.TimeUnit;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description
 **/
public interface ICacheAdapter {
    String get(String key);

    void set(String key, String value);

    void set(String key, String value, long timeout, TimeUnit timeUnit);

    void del(String key);
}
