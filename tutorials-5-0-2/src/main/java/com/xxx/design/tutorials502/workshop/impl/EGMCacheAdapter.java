package com.xxx.design.tutorials502.workshop.impl;

import com.xxx.design.tutorials500.redis.cluster.EGM;
import com.xxx.design.tutorials502.workshop.ICacheAdapter;

import java.util.concurrent.TimeUnit;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
<<<<<<< HEAD
 * @description
=======
 * @description 基于接口实现EGM适配
>>>>>>> d2133f2 (第十一讲 理解并完成建造者模式的使用)
 **/
public class EGMCacheAdapter implements ICacheAdapter {
    private EGM egm = new EGM();

    @Override
    public String get(String key) {
        return egm.gain(key);
    }

    @Override
    public void set(String key, String value) {
        egm.set(key,value);
    }

    @Override
    public void set(String key, String value, long timeout, TimeUnit timeUnit) {
        egm.setEx(key,value,timeout,timeUnit);
    }

    @Override
    public void del(String key) {
        egm.delete(key);
    }
}
