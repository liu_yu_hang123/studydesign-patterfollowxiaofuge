package com.xxx.design.tutorials502.workshop.impl;

import com.xxx.design.tutorials500.redis.cluster.IIR;
import com.xxx.design.tutorials502.workshop.ICacheAdapter;

import java.util.concurrent.TimeUnit;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
<<<<<<< HEAD
 * @description
=======
 * @description 基于接口实现IIR的适配
>>>>>>> d2133f2 (第十一讲 理解并完成建造者模式的使用)
 **/
public class IIRCacheAdapter implements ICacheAdapter {
    private IIR iir = new IIR();
    @Override
    public String get(String key) {
        return iir.get(key);
    }

    @Override
    public void set(String key, String value) {
        iir.set(key,value);
    }

    @Override
    public void set(String key, String value, long timeout, TimeUnit timeUnit) {
        iir.setExpire(key,value,timeout,timeUnit);
    }

    @Override
    public void del(String key) {
        iir.del(key);
    }
}
