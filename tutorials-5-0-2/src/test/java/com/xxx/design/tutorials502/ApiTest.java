package com.xxx.design.tutorials502;

import com.xxx.design.tutorials500.application.CacheService;
import com.xxx.design.tutorials502.factory.JDKProxyFactory;
import com.xxx.design.tutorials502.workshop.impl.EGMCacheAdapter;
import com.xxx.design.tutorials502.workshop.impl.IIRCacheAdapter;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description 抽象工厂测试
 **/
public class ApiTest {
    private Logger logger = LoggerFactory.getLogger(ApiTest.class);

    @Test
    public void test_CacheService() throws Exception {
        CacheService proxy_EGM = JDKProxyFactory.getProxy(CacheService.class,EGMCacheAdapter.class);
        proxy_EGM.set("username_01","xiaoliu");
        String value1 = proxy_EGM.get("username_01");
        logger.info("缓存服务 EGM 测试，proxy_EGM.get 测试结果：{}", value1);

        CacheService proxy_IIR = JDKProxyFactory.getProxy(CacheService.class,IIRCacheAdapter.class);
        proxy_IIR.set("username_01","xiaoyu");
        String value2 = proxy_IIR.get("username_01");
        logger.info("缓存服务 EGM 测试，proxy_EGM.get 测试结果：{}", value2);
    }
}
