package com.xxx.design.tutorials600;

import java.math.BigDecimal;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description  装修材料接口
 **/
public interface Matter {
    String scene(); // 场景；地砖、地板、涂料、吊顶

    String brand(); // 品牌

    String model(); // 型号

    BigDecimal price(); // 价格

    String desc(); // 描述
}
