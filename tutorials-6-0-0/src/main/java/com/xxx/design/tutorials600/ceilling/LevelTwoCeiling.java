package com.xxx.design.tutorials600.ceilling;

import com.xxx.design.tutorials600.Matter;

import java.math.BigDecimal;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description
 **/
public class LevelTwoCeiling implements Matter {
    @Override
    public String scene() {
        return "吊顶";
    }

    @Override
    public String brand() {
        return "装修公司自带";
    }

    @Override
    public String model() {
        return "二级顶";
    }

    @Override
    public BigDecimal price() {
        return new BigDecimal(850);
    }

    @Override
    public String desc() {
        return "两个层次的吊顶，二级吊顶高度一般就往下吊20cm，钥匙层高很高，也可增加每层的厚度";
    }
}
