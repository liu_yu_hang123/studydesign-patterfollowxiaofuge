package com.xxx.design.tutorials602;

import com.xxx.design.tutorials600.ceilling.LevelOneCeiling;
import com.xxx.design.tutorials600.ceilling.LevelTwoCeiling;
import com.xxx.design.tutorials600.coat.DuluxCoat;
import com.xxx.design.tutorials600.coat.LiBangCoat;
import com.xxx.design.tutorials600.floor.ShengXiangFloor;
import com.xxx.design.tutorials600.tile.DongPengTile;
import com.xxx.design.tutorials600.tile.MarcoPoloTile;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description
 **/
public class Builder {
    public IMenu levelOne(Double area){
        return new DecorationPackageMenu(area,"豪华欧式")
                .appendCeiling(new LevelTwoCeiling()) //吊顶，二级顶
                .appendCoat(new DuluxCoat())    //涂料，多乐士
                .appendFloor(new ShengXiangFloor()); //地板，圣象
    }

    public  IMenu levelTwo(Double area){
        return new DecorationPackageMenu(area,"轻奢田园")
                .appendCeiling(new LevelTwoCeiling()) //吊顶，二级顶
                .appendCoat(new LiBangCoat())
                .appendCoat(new DuluxCoat())//涂料，立邦
                .appendTile(new MarcoPoloTile()); //地砖，马可波罗
    }

    public IMenu levelThree(Double area){
        return new DecorationPackageMenu(area,"现代简约")
                .appendCeiling(new LevelOneCeiling()) //吊顶，二级顶
                .appendCoat(new LiBangCoat()) // 涂料，立邦
                .appendTile(new DongPengTile()); // 地砖，东鹏
    }
}
