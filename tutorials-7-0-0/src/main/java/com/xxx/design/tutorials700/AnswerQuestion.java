package com.xxx.design.tutorials700;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description
 **/

/**
 * 解答题
 */
public class AnswerQuestion {
    private String name; // 问题
    private String key; // 答案

    public AnswerQuestion() {
    }
    public AnswerQuestion(String name,String key){
        this.name = name;
        this.key = key;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getKey(){
        return key;
    }
    public void setKey(String key){
        this.key = key;
    }
}
