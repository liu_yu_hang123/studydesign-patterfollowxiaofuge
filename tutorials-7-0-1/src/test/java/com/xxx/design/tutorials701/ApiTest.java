package com.xxx.design.tutorials701;

import org.junit.jupiter.api.Test;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description 分发给每个人一模一样的试卷
 **/
public class ApiTest {
    @Test
    public void test_QuestionBankController() {
        QuestionBankController questionBankController = new QuestionBankController();
        System.out.println(questionBankController.createPaper("花花", "1000001921032"));
        System.out.println(questionBankController.createPaper("豆豆", "1000001921051"));
        System.out.println(questionBankController.createPaper("大宝", "1000001921987"));
    }
}
