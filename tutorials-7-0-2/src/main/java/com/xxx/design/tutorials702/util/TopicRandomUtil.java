package com.xxx.design.tutorials702.util;

import java.util.*;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description
 **/

public class TopicRandomUtil {
    /**
     * 乱序Map元素，记录对应答案key
     * @param option 题目
     * @param key    答案
     * @return Topic 乱序后 {A=c., B=d., C=a., D=b.}
     */
    public static Topic random(Map<String,String> option,String key){
        Set<String> keyset = option.keySet(); // key集合
        ArrayList<String> keyList = new ArrayList<>(keyset); // 使用列表进行乱序
        // 把选项顺序打乱，keyList的内容是乱的
        Collections.shuffle(keyList);
        HashMap<String, String> optionNew = new HashMap<>();
        int idx = 0;
        String keyNew = "";
        for(String next:keyset){ // keySet不乱，还是abcd
            String randomKey = keyList.get(idx++);
            // 但是值还是对应的，记录考试题目的答案
            if(key.equals(next)){
                keyNew = randomKey;
            }
            optionNew.put(randomKey,option.get(next));
        }
        return new Topic(optionNew,keyNew);
    }
}
