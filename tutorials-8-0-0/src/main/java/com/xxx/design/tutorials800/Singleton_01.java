package com.xxx.design.tutorials800;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description 懒汉模式（线程不安全）
 **/
public class Singleton_01 {
    private static Singleton_01 instance;

    private Singleton_01() {
    }

    public static Singleton_01 getInstance(){
        if(null != instance) return instance;
        return new Singleton_01();
    }

    public static void main(String[] args) {
        System.out.println(Singleton_01.getInstance()); // org.itstack.demo.design.Singleton_06@2b193f2d
        System.out.println(Singleton_01.getInstance()); // org.itstack.demo.design.Singleton_06@2b193f2d
    }
}
