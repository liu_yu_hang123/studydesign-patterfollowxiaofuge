package com.xxx.design.tutorials800;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description 懒汉模式（线程安全）
 **/
public class Singleton_02 {
    private static Singleton_02 instance;

    private Singleton_02(){}

    // 锁的粒度过大
    public static synchronized Singleton_02 getInstance(){
        if(null != instance) return instance;
        return new Singleton_02();
    }

    public static void main(String[] args) {
        System.out.println(Singleton_02.getInstance()); // org.itstack.demo.design.Singleton_06@2b193f2d
        System.out.println(Singleton_02.getInstance()); // org.itstack.demo.design.Singleton_06@2b193f2d
    }
}
