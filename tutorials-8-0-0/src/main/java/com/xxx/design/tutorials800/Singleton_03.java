package com.xxx.design.tutorials800;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description 饿汉式（线程安全）
 **/
public class Singleton_03 {
    private static Singleton_03 instance = new Singleton_03();

    private Singleton_03(){
    }

    public static Singleton_03 getInstance(){
        return instance;
    }
    public static void main(String[] args) {
        System.out.println(Singleton_03.getInstance()); // org.itstack.demo.design.Singleton_06@2b193f2d
        System.out.println(Singleton_03.getInstance()); // org.itstack.demo.design.Singleton_06@2b193f2d
    }
}
