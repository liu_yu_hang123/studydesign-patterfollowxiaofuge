package com.xxx.design.tutorials800;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description 类的内部类
 **/
public class Singleton_04 {
    private static class SingletonHolder{
        private static Singleton_04 instance = new Singleton_04();
    }

    private Singleton_04(){}

    public static Singleton_04 getInstance(){
        return SingletonHolder.instance;
    }
    public static void main(String[] args) {
        System.out.println(Singleton_04.getInstance()); // org.itstack.demo.design.Singleton_06@2b193f2d
        System.out.println(Singleton_04.getInstance()); // org.itstack.demo.design.Singleton_06@2b193f2d
    }
}
