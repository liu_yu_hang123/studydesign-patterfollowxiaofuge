package com.xxx.design.tutorials800;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description 双重锁检验(Double Check Lock)
 **/
public class Singleton_05 {
    private static volatile Singleton_05 instance;

    private Singleton_05(){}

    public static Singleton_05 getInstance(){
        if(null != instance) return instance;
        synchronized (Singleton_05.class){
            if(null == instance){
                instance =  new Singleton_05();
            }
        }
        return instance;
    }

    public static void main(String[] args) {
        System.out.println(Singleton_05.getInstance()); // org.itstack.demo.design.Singleton_06@2b193f2d
        System.out.println(Singleton_05.getInstance()); // org.itstack.demo.design.Singleton_06@2b193f2d
    }
}
