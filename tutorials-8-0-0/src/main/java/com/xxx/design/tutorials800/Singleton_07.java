package com.xxx.design.tutorials800;

/**
 * @author Hang
 * @date 2022/7/30
 * @project studydesignpatter
 * @description
 **/
public enum Singleton_07 {
    INSTANCE;
    public void  test(){
        System.out.println("hi~");
    }
}
