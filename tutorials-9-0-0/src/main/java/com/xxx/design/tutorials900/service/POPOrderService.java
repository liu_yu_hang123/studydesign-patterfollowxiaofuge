package com.xxx.design.tutorials900.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Hang
 * @date 2022/7/31
 * @project studydesignpatter
 * @description
 **/
public class POPOrderService {

    private Logger logger = LoggerFactory.getLogger(POPOrderService.class);

    public boolean isFirstOrder(String uId) {
        logger.info("POP商家，查询用户的订单是否为首单：{}", uId);
        return true;
    }

}
