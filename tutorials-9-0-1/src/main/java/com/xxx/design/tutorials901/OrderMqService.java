package com.xxx.design.tutorials901;

import com.alibaba.fastjson.JSON;
import com.xxx.design.tutorials900.mq.OrderMq;

/**
 * @author Hang
 * @date 2022/7/31
 * @project studydesignpatter
 * @description
 **/
public class OrderMqService {
    public void onMessage(String message) {

        OrderMq mq = JSON.parseObject(message, OrderMq.class);

        mq.getUid();
        mq.getOrderId();
        mq.getCreateOrderTime();

        // ... 处理自己的业务
    }
}
