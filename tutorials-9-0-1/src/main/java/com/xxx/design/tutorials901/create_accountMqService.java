package com.xxx.design.tutorials901;

import com.alibaba.fastjson.JSON;
import com.xxx.design.tutorials900.mq.create_account;

/**
 * @author Hang
 * @date 2022/7/31
 * @project studydesignpatter
 * @description
 **/
public class create_accountMqService {
    public void onMessage(String message) {

        create_account mq = JSON.parseObject(message, create_account.class);

        mq.getNumber();
        mq.getAccountDate();

        // ... 处理自己的业务
    }
}
