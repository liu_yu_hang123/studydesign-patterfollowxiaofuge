package com.xxx.design.tutorials902;

/**
 * @author Hang
 * @date 2022/7/31
 * @project studydesignpatter
 * @description
 **/
public interface OrderAdapterService {
    boolean isFirst(String uId);
}
