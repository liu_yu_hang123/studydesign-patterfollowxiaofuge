package com.xxx.design.tutorials902.impl;

import com.xxx.design.tutorials900.service.OrderService;
import com.xxx.design.tutorials902.OrderAdapterService;

/**
 * @author Hang
 * @date 2022/7/31
 * @project studydesignpatter
 * @description 内部订单，判断首单逻辑
 **/
public class InsideOrderServiceImpl implements OrderAdapterService {
    private OrderService orderService = new OrderService();

    public boolean isFirst(String uId) {
        return orderService.queryUserOrderCount(uId) <= 1;
    }
}
