package com.xxx.design.tutorials902.impl;

import com.xxx.design.tutorials900.service.POPOrderService;
import com.xxx.design.tutorials902.OrderAdapterService;

/**
 * @author Hang
 * @date 2022/7/31
 * @project studydesignpatter
 * @description 第三方订单，判断首单逻辑
 **/
public class POPOrderAdapterServiceImpl implements OrderAdapterService {
    private POPOrderService popOrderService = new POPOrderService();

    public boolean isFirst(String uId) {
        return popOrderService.isFirstOrder(uId);
    }
}
